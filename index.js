const openBtn = document.querySelector('#memo-open-btn');
const closeBtn = document.querySelector('.closeBtn');
const inputContainer = document.querySelector('.input-container');

openBtn.addEventListener('click', () => {
  inputContainer.style.display = 'block';
})

closeBtn.addEventListener('click', () => {
  inputContainer.style.display = 'none';
})




const authorInput = document.getElementById('author-input');
const titleInput = document.getElementById('title-input');
const memoInput = document.getElementById('memo-input');


const writeBtn = document.querySelector('.writeBtn');

const getAllMemos = async () => {
  fetch('http://localhost:8080/memos')
    .then((response) => response.json())
    .then((memos) => memos.forEach(({author, title, content}) => {
      // console.log({author, title, content});
      const elem = `<div class="memo-card-item">
      <h2>${title}</h2>
      <h3>${author}</h3>
      <p>${content}</p>
    </div>`;
      document.querySelector('.cards').innerHTML +=  elem;
    }));
};

document.addEventListener('DOMContentLoaded', () => {
  getAllMemos();
});
